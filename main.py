# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import logging
import os
import pytz
import string
import random as rand
from pytz import timezone
from urllib.parse import unquote

from flask import Flask, render_template, request, Response
import sqlalchemy

app = Flask(__name__)
user_tz = timezone('Asia/Bangkok')

logger = logging.getLogger()


def init_connection_engine():
    db_config = {
        # [START cloud_sql_mysql_sqlalchemy_limit]
        # Pool size is the maximum number of permanent connections to keep.
        "pool_size": 5,
        # Temporarily exceeds the set pool_size if no connections are available.
        "max_overflow": 2,
        # The total number of concurrent connections for your application will be
        # a total of pool_size and max_overflow.
        # [END cloud_sql_mysql_sqlalchemy_limit]
        # [START cloud_sql_mysql_sqlalchemy_backoff]
        # SQLAlchemy automatically uses delays between failed connection attempts,
        # but provides no arguments for configuration.
        # [END cloud_sql_mysql_sqlalchemy_backoff]
        # [START cloud_sql_mysql_sqlalchemy_timeout]
        # 'pool_timeout' is the maximum number of seconds to wait when retrieving a
        # new connection from the pool. After the specified amount of time, an
        # exception will be thrown.
        "pool_timeout": 30,  # 30 seconds
        # [END cloud_sql_mysql_sqlalchemy_timeout]
        # [START cloud_sql_mysql_sqlalchemy_lifetime]
        # 'pool_recycle' is the maximum number of seconds a connection can persist.
        # Connections that live longer than the specified amount of time will be
        # reestablished
        "pool_recycle": 1800,  # 30 minutes
        # [END cloud_sql_mysql_sqlalchemy_lifetime]
    }

    if os.environ.get("DB_HOST"):
        return init_tcp_connection_engine(db_config)
    else:
        return init_unix_connection_engine(db_config)


def init_tcp_connection_engine(db_config):
    # [START cloud_sql_mysql_sqlalchemy_create_tcp]
    # Remember - storing secrets in plaintext is potentially unsafe. Consider using
    # something like https://cloud.google.com/secret-manager/docs/overview to help keep
    # secrets secret.
    db_user = os.environ["DB_USER"]
    db_pass = os.environ["DB_PASS"]
    db_name = os.environ["DB_NAME"]
    db_host = os.environ["DB_HOST"]

    # Extract host and port from db_host
    host_args = db_host.split(":")
    db_hostname, db_port = host_args[0], int(host_args[1])

    pool = sqlalchemy.create_engine(
        # Equivalent URL:
        # mysql+pymysql://<db_user>:<db_pass>@<db_host>:<db_port>/<db_name>
        sqlalchemy.engine.url.URL(
            drivername="mysql+pymysql",
            username=db_user,  # e.g. "my-database-user"
            password=db_pass,  # e.g. "my-database-password"
            host=db_hostname,  # e.g. "127.0.0.1"
            port=db_port,  # e.g. 3306
            database=db_name,  # e.g. "my-database-name"
        ),
        # ... Specify additional properties here.
        # [END cloud_sql_mysql_sqlalchemy_create_tcp]
        **db_config
        # [START cloud_sql_mysql_sqlalchemy_create_tcp]
    )
    # [END cloud_sql_mysql_sqlalchemy_create_tcp]

    return pool


def init_unix_connection_engine(db_config):
    # [START cloud_sql_mysql_sqlalchemy_create_socket]
    # Remember - storing secrets in plaintext is potentially unsafe. Consider using
    # something like https://cloud.google.com/secret-manager/docs/overview to help keep
    # secrets secret.
    db_user = os.environ["DB_USER"]
    db_pass = os.environ["DB_PASS"]
    db_name = os.environ["DB_NAME"]
    db_socket_dir = os.environ.get("DB_SOCKET_DIR", "/cloudsql")
    cloud_sql_connection_name = os.environ["CLOUD_SQL_CONNECTION_NAME"]

    pool = sqlalchemy.create_engine(
        # Equivalent URL:
        # mysql+pymysql://<db_user>:<db_pass>@/<db_name>?unix_socket=<socket_path>/<cloud_sql_instance_name>
        sqlalchemy.engine.url.URL(
            drivername="mysql+pymysql",
            username=db_user,  # e.g. "my-database-user"
            password=db_pass,  # e.g. "my-database-password"
            database=db_name,  # e.g. "my-database-name"
            query={
                "unix_socket": "{}/{}".format(
                    db_socket_dir,  # e.g. "/cloudsql"
                    cloud_sql_connection_name)  # i.e "<PROJECT-NAME>:<INSTANCE-REGION>:<INSTANCE-NAME>"
            }
        ),
        # ... Specify additional properties here.

        # [END cloud_sql_mysql_sqlalchemy_create_socket]
        **db_config
        # [START cloud_sql_mysql_sqlalchemy_create_socket]
    )
    # [END cloud_sql_mysql_sqlalchemy_create_socket]

    return pool


# The SQLAlchemy engine will help manage interactions, including automatically
# managing a pool of connections to your database
db = init_connection_engine()

def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(rand.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(stringLength))

def queryFixPwdUserInfo(username):
    query = "Select username, pwd From userfixpassword Where username=\'" + username + "\'"
    data = []
    result = []
    with db.connect() as conn:
        # Execute the query and fetch all results
        data = conn.execute(query).fetchall()
        # # Convert the results into a list of dicts representing votes
        for row in data:
            records = []
            for col in row:
                text = ''
                if isinstance(col, datetime.datetime):
                    text = col.strftime("%Y-%m-%d %H:%M:%S")
                elif col is not None:
                    text = col
                records.append(text)
            result.append(records)

    return result

def getPassword(username):
    pwd = randomString()+str(rand.randint(1,9)) #Random Password
    fixPwdUsers = queryFixPwdUserInfo(username)
    print(fixPwdUsers)
    # users = []
    # for c in fixPwdUsers:
    #     user = {}
    #     user['username'] = str(c[0])
    #     user['pwd'] = str(c[1])
    #     users.append(user)

    if len(fixPwdUsers) > 0:
        pwd = fixPwdUsers[0][1]
    return pwd

@app.route("/createTable", methods=["GET"])
def create_tables():
    # Create tables (if they don't already exist)
    with db.connect() as conn:
        conn.execute(
            "CREATE TABLE IF NOT EXISTS userinfo "
            "( username VARCHAR(255) NOT NULL, pwd VARCHAR(255), createddate DATETIME, lastmodifieddate DATETIME, "
            "lastlogindate DATETIME, synclum VARCHAR(6), syncsf VARCHAR(6), PRIMARY KEY (username) );"
        )

        conn.execute(
            "CREATE TABLE IF NOT EXISTS loginhistory "
            "( id SERIAL NOT NULL, username VARCHAR(255) NOT NULL, LineUid VARCHAR(255), Status VARCHAR(255), Timestamp  DATETIME NOT NULL, "
            "channel VARCHAR(255), PRIMARY KEY (id) );"
        )

        conn.execute(
            "CREATE TABLE IF NOT EXISTS emaillog "
            "( email_id SERIAL NOT NULL, from_address VARCHAR(255), htmlbody TEXT, Timestamp DATETIME NOT NULL, "
            "subject VARCHAR(255) NOT NULL, to_address VARCHAR(255) NOT NULL, textmsg TEXT, syncResult VARCHAR(6), "
            "PRIMARY KEY (email_id) );"
        )

    return Response(
        status=200,
        response="CREATE TABLE userinfo, loginhistory, emaillog was Successful.",
    )

@app.route("/createNewTable", methods=["GET"])
def create_new_tables():
    # Create tables (if they don't already exist)
    authen = request.headers.get("Authorization")
    if authen != 'Bearer o4VoxqDpDEpal78dKi983YTi84vKl30iypPOuye862O':
        print ('Authentication was failed!')
        return Response(
            status=400,
            response="Authentication was failed!",
        )

    tablename = request.args.get("tablename")
    fields = request.args.get("fields")
    # requestdata = request.get_json()
    # for data in requestdata:
    #     tablename = data['tablename']
    #     fields = data['fields']

    with db.connect() as conn:
        conn.execute(
            "CREATE TABLE IF NOT EXISTS " + tablename + " " + fields + ";"
        )

    return Response(
        status=200,
        response="CREATE NEW TABLE " + tablename + " was Successful.",
    )

@app.route("/dropTable", methods=["GET"])
def drop_table():
    tablename = request.args.get("tablename")
    with db.connect() as conn:
        conn.execute(
            "DROP TABLE IF EXISTS " + tablename + ";"
        )

    return Response(
        status=200,
        response="DROP TABLE " + tablename + " was Successful.",
    )


@app.route("/", methods=["GET"])
def query():
    authen = request.headers.get("Authorization")
    if authen != 'Bearer o4VoxqDpDEpal78dKi983YTi84vKl30iypPOuye862O':
        print ('Authentication was failed!')
        return Response(
            status=400,
            response="Authentication was failed!",
        )

    query = request.args.get("query")
    print('query: ' + query)
    # query = "Select username, pwd, createddate, lastmodifieddate, lastlogindate, synclum, syncsf from userinfo Where username LIKE \'1283237%\'"
    data = []
    result = ''
    with db.connect() as conn:
        # Execute the query and fetch all results
        data = conn.execute(query).fetchall()
        # # Convert the results into a list of dicts representing votes
        for row in data:
            firstCol = True
            for col in row:
                text = ''
                if firstCol == True:
                    firstCol = False
                else:
                    result += ','
                if isinstance(col, datetime.datetime):
                    text = col.replace(tzinfo=pytz.utc).astimezone(user_tz).strftime("%Y-%m-%d %H:%M:%S")
                elif isinstance(col, (int, float)):
                    text = str(col)
                elif col is not None:
                    text = col
                result += ('\"' + text + '\"')

            result += '\r\n'

    return result

@app.route("/getFixPwdUsers", methods=["GET"])
def getFixPwdUsers():
    authen = request.headers.get("Authorization")
    if authen != 'Bearer o4VoxqDpDEpal78dKi983YTi84vKl30iypPOuye862O':
        print ('Authentication was failed!')
        return Response(
            status=400,
            response="Authentication was failed!",
        )

    uname = request.args.get("username")
    pwd = getPassword(uname)
    return Response(
        status=200,
        response="Password of Username " + uname +  " is : " + pwd
    )

@app.route("/deleteRecord", methods=["GET"])
def deleteRecord():
    authen = request.headers.get("Authorization")
    if authen != 'Bearer o4VoxqDpDEpal78dKi983YTi84vKl30iypPOuye862O':
        print ('Authentication was failed!')
        return Response(
            status=400,
            response="Authentication was failed!",
        )

    tablename = request.args.get("tablename")
    conditon = request.args.get("condition")
    print('conditon: ' + conditon)
    # query = "Select username, pwd, createddate, lastmodifieddate, lastlogindate, synclum, syncsf from userinfo Where username LIKE \'1283237%\'"
    with db.connect() as conn:
        # Execute the query and fetch all results
        conn.execute("DELETE FROM " + tablename + " WHERE " + conditon.replace(':', '='))
        # # Convert the results into a list of dicts representing votes
    return Response(
        status=200,
        response="Delete Record " + tablename + " " + conditon.replace(':', '=') +" was Successful.",
    )


@app.route("/addUserInfo", methods=["POST"])
def addUserInfo():
    authen = request.headers.get("Authorization")
    if authen != 'Bearer o4VoxqDpDEpal78dKi983YTi84vKl30iypPOuye862O':
        print ('Authentication was failed!')
        return Response(
            status=400,
            response="Authentication was failed!",
        )

    # Get the team and time the vote was cast.
    requestdata = request.get_json()
    # print('requestData: ' + requestdata)
    inputValue = ''

    for data in requestdata:
        username = data['username']
        pwd = data['pwd']
        createddate = data['createddate']
        lastmodifieddate = data['lastmodifieddate']
        lastlogindate = data['lastlogindate']
        synclum = data['synclum']
        syncsf = data['syncsf']

        if lastmodifieddate == '':
            lastmodifieddate = 'NULL'
        else:
            lastmodifieddate = "\'" + lastmodifieddate + "\'"
        if lastlogindate == '':
            lastlogindate = 'NULL'
        else:
            lastlogindate = "\'" + lastlogindate + "\'"

        datatext = "(\'" + username + "\',\'" + pwd + "\',\'" + createddate + "\'," + lastmodifieddate + "," + lastlogindate + ",\'" + synclum.upper() + "\',\'" + syncsf.upper() + "\')"

        if inputValue != "":
            inputValue += ','

        inputValue += datatext

    # Verify that the team is one of the allowed options
    if inputValue == "":
        print ('input value is null!')
        return Response(response="input value is null!", status=400)

    # [START cloud_sql_mysql_sqlalchemy_connection]
    # Preparing a statement before hand can help protect against injections.
    sqlupsert = "INSERT INTO userinfo (username, pwd, createddate, lastmodifieddate, lastlogindate, synclum, syncsf)"
    sqlupsert += " VALUES " + inputValue
    sqlupsert += " ON DUPLICATE KEY UPDATE"
    sqlupsert += " pwd=VALUES(pwd), lastmodifieddate=VALUES(lastmodifieddate), lastlogindate=VALUES(lastlogindate),"
    sqlupsert += " synclum=VALUES(synclum), syncsf=VALUES(syncsf);"
    print('sqlupsert: ' + sqlupsert)
    # stmt = sqlalchemy.text(sqlupsert)
    try:
        # Using a with statement ensures that the connection is always released
        # back into the pool at the end of statement (even if an error occurs)
        with db.connect() as conn:
            conn.execute(sqlupsert)
    except Exception as e:
        # If something goes wrong, handle the error in this section. This might
        # involve retrying or adjusting parameters depending on the situation.
        # [START_EXCLUDE]
        print ('Upsert UserInfo was failed! ' + str(e))
        return Response(
            status=500,
            response="Upsert UserInfo was failed! " + str(e)
        )
        # [END_EXCLUDE]
    # [END cloud_sql_mysql_sqlalchemy_connection]

    return Response(
        status=200,
        response="Upsert UserInfo was Successful.",
    )

@app.route("/addUserFixPwd", methods=["POST"])
def addUserFixPwd():
    authen = request.headers.get("Authorization")
    if authen != 'Bearer o4VoxqDpDEpal78dKi983YTi84vKl30iypPOuye862O':
        print ('Authentication was failed!')
        return Response(
            status=400,
            response="Authentication was failed!",
        )

    # Get the team and time the vote was cast.
    requestdata = request.get_json()
    # print('requestData: ' + requestdata)
    inputValue = ''

    for data in requestdata:
        username = data['username']
        pwd = data['pwd']
        createddate = data['createddate']
        lastmodifieddate = data['lastmodifieddate']

        if lastmodifieddate == '':
            lastmodifieddate = 'NULL'
        else:
            lastmodifieddate = "\'" + lastmodifieddate + "\'"

        datatext = "(\'" + username + "\',\'" + pwd + "\',\'" + createddate + "\'," + lastmodifieddate + ")"

        if inputValue != "":
            inputValue += ','

        inputValue += datatext

    # Verify that the team is one of the allowed options
    if inputValue == "":
        print ('input value is null!')
        return Response(response="input value is null!", status=400)

    # [START cloud_sql_mysql_sqlalchemy_connection]
    # Preparing a statement before hand can help protect against injections.
    sqlupsert = "INSERT INTO userfixpassword (username, pwd, createddate, lastmodifieddate)"
    sqlupsert += " VALUES " + inputValue
    sqlupsert += " ON DUPLICATE KEY UPDATE"
    sqlupsert += " pwd=VALUES(pwd), lastmodifieddate=VALUES(lastmodifieddate);"
    print('sqlupsert: ' + sqlupsert)
    # stmt = sqlalchemy.text(sqlupsert)
    try:
        # Using a with statement ensures that the connection is always released
        # back into the pool at the end of statement (even if an error occurs)
        with db.connect() as conn:
            conn.execute(sqlupsert)
    except Exception as e:
        # If something goes wrong, handle the error in this section. This might
        # involve retrying or adjusting parameters depending on the situation.
        # [START_EXCLUDE]
        print ('Upsert UserFixPwd was failed! ' + str(e))
        return Response(
            status=500,
            response="Upsert UserFixPwd was failed! " + str(e)
        )
        # [END_EXCLUDE]
    # [END cloud_sql_mysql_sqlalchemy_connection]

    return Response(
        status=200,
        response="Upsert UserFixPwd was Successful.",
    )

@app.route("/addLoginHistory", methods=["POST"])
def addLoginHistory():
    authen = request.headers.get("Authorization")
    if authen != 'Bearer o4VoxqDpDEpal78dKi983YTi84vKl30iypPOuye862O':
        print ('Authentication was failed!')
        return Response(
            status=400,
            response="Authentication was failed!"
        )

    # Get the team and time the vote was cast.
    requestdata = request.get_json()
    # print('requestData: ' + requestdata)
    inputValue = ''

    for data in requestdata:
        username = data['username']
        LineUid = data['LineUid']
        Status = data['Status']
        channel = data['channel']
        timestamp = data['timestamp']

        if timestamp == '':
            timestamp = 'NULL'
        else:
            timestamp = "\'" + timestamp + "\'"

        datatext = "(\'" + username + "\',\'" + LineUid + "\',\'" + Status + "\',\'" + channel + "\'," + timestamp + ")"

        if inputValue != "":
            inputValue += ','

        inputValue += datatext

    # Verify that the team is one of the allowed options
    if inputValue == "":
        print ('input value is null!')
        return Response(response="input value is null!", status=400)

    # [START cloud_sql_mysql_sqlalchemy_connection]
    # Preparing a statement before hand can help protect against injections.
    sqlInsert = "INSERT INTO loginhistory (username, LineUid, Status, channel, Timestamp)"
    sqlInsert += " VALUES " + inputValue
    # sqlupsert += " ON DUPLICATE KEY UPDATE"
    # sqlupsert += " pwd=VALUES(pwd), lastmodifieddate=VALUES(lastmodifieddate), lastlogindate=VALUES(lastlogindate),"
    # sqlupsert += " synclum=VALUES(synclum), syncsf=VALUES(syncsf);"
    print('sqlInsert: ' + sqlInsert)
    # stmt = sqlalchemy.text(sqlupsert)
    try:
        # Using a with statement ensures that the connection is always released
        # back into the pool at the end of statement (even if an error occurs)
        with db.connect() as conn:
            conn.execute(sqlInsert)
    except Exception as e:
        # If something goes wrong, handle the error in this section. This might
        # involve retrying or adjusting parameters depending on the situation.
        # [START_EXCLUDE]
        print ('Insert loginhistory was failed! ' + str(e))
        return Response(
            status=500,
            response="Insert loginhistory was failed! "  + str(e)
        )
        # [END_EXCLUDE]
    # [END cloud_sql_mysql_sqlalchemy_connection]

    return Response(
        status=200,
        response="Insert loginhistory was Successful.",
    )

@app.route("/addEmialLogs", methods=["POST"])
def addEmialLogs():
    authen = request.headers.get("Authorization")
    if authen != 'Bearer o4VoxqDpDEpal78dKi983YTi84vKl30iypPOuye862O':
        print ('Authentication was failed!')
        return Response(
            status=400,
            response="Authentication was failed!",
        )

    # Get the team and time the vote was cast.
    requestdata = request.get_json()
    # print('requestData: ' + requestdata)
    inputValue = ''

    for data in requestdata:
        from_address = data['from_address']
        htmlbody = unquote(data['htmlbody'])
        timestamp = data['Timestamp']
        subject = data['subject']
        to_address = data['to_address']
        textmsg = unquote(data['textmsg'])
        syncResult = data['syncResult']

        if timestamp == '':
            timestamp = 'NULL'
        else:
            timestamp = "\'" + timestamp + "\'"

        datatext = "(\'" + from_address + "\',\'" + htmlbody + "\'," + timestamp + ",\'" + subject + "\',\'" + to_address + "\',\'" + textmsg + "\',\'" + syncResult.upper() + "\')"

        if inputValue != "":
            inputValue += ','

        inputValue += datatext

    # Verify that the team is one of the allowed options
    if inputValue == "":
        print ('input value is null!')
        return Response(response="input value is null!", status=400)

    # [START cloud_sql_mysql_sqlalchemy_connection]
    # Preparing a statement before hand can help protect against injections.
    sqlInsert = "INSERT INTO emaillog (from_address, htmlbody, Timestamp, subject, to_address, textmsg, syncResult)"
    sqlInsert += " VALUES " + inputValue
    # sqlupsert += " ON DUPLICATE KEY UPDATE"
    # sqlupsert += " pwd=VALUES(pwd), lastmodifieddate=VALUES(lastmodifieddate), lastlogindate=VALUES(lastlogindate),"
    # sqlupsert += " synclum=VALUES(synclum), syncsf=VALUES(syncsf);"
    print('sqlInsert: ' + sqlInsert)
    # stmt = sqlalchemy.text(sqlupsert)
    try:
        # Using a with statement ensures that the connection is always released
        # back into the pool at the end of statement (even if an error occurs)
        with db.connect() as conn:
            conn.execute(sqlInsert)
    except Exception as e:
        # If something goes wrong, handle the error in this section. This might
        # involve retrying or adjusting parameters depending on the situation.
        # [START_EXCLUDE]
        print ('Insert emaillog was failed! ' + str(e))
        return Response(
            status=500,
            response="Insert emaillog was failed! " + str(e)
        )
        # [END_EXCLUDE]
    # [END cloud_sql_mysql_sqlalchemy_connection]

    return Response(
        status=200,
        response="Insert emaillog was Successful.",
    )

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8282, debug=True)